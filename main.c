/* QI 5W "Dumb" Receiver
 *  modulates a roughly 2KHz digital wave, ignores TX FM mod (for now)
 *  and checks to see if pings being output from TX (not yet)
*/

#define LOADMOD_PIN 6
#define APOW_PIN A3


// global variables bc im lazy
int power_read = 0;
volatile uint8_t bit_state = 0;
static int POW_DETECT_THRESHOLD = 700;
volatile long long print_cd = 0;


void setup() {
    // this code needs to enable prints (for my sanity)
    // as well as enabling the modulating port (pin 6? check for green wire)
    pinMode(LOADMOD_PIN, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.begin(9600); // open the serial port at 9600 bps
}

void print_with_cd(uint16_t val, bool ignore = true) {
    print_cd++;
    if (print_cd > 100000 or ignore) {
        if (ignore) {
            Serial.print("F");
        }
        Serial.print("Printed value...");    // prints a tab
        Serial.print(val);    // prints a tab
        Serial.println();        // prints a carriage return
        print_cd = 0;
    }
}

void adc_init() {
    // as far as we're currently aware, arduino inits your ADCs for you
    // so nothing
}

uint16_t adc_read(void) {
    // same goes for reading from the ADC, we don't seem to need more than std read
    return analogRead(APOW_PIN);
}

void tx_byte(uint8_t data) {
    // this function transmits 11 bit packets, containing:
    // start bit (0)

    // 8 data bits
    // parity
    // stop
    bit_state ^= 1;
    // zero bit is always 0, so hold for 2 cycles (500us)
    digitalWrite(LOADMOD_PIN, bit_state);
    delayMicroseconds(250);
    digitalWrite(LOADMOD_PIN, bit_state);
    delayMicroseconds(250);

    // transmit the 8 bits
    uint8_t parity = 0;
    for (int i = 0; i < 8; i++) {
        bit_state ^= 1;
        digitalWrite(LOADMOD_PIN, bit_state);
        delayMicroseconds(250);
        if (data & (1 << i)) {
            parity++;
            bit_state ^= 1;
        }
        digitalWrite(LOADMOD_PIN, bit_state);
        delayMicroseconds(250);
    }

    // flip the parity again? idk what the dude doing
    if (parity & 1) {
        parity = 0;
    } else
        parity = 1;

    bit_state ^= 1;
    digitalWrite(LOADMOD_PIN, bit_state);
    delayMicroseconds(250);

    if (parity) {
        bit_state ^= 1;
    }
    digitalWrite(LOADMOD_PIN, bit_state);
    delayMicroseconds(250);

    // lastly, transmit stop bit of 1
    bit_state ^= 1;
    digitalWrite(LOADMOD_PIN, bit_state);
    delayMicroseconds(250);
    bit_state ^= 1;

    digitalWrite(LOADMOD_PIN, bit_state);
    delayMicroseconds(250);
}

void tx(uint8_t * data, int len) {
    // this function takes multiple bytes and transmits them consecutively
    uint8_t checksum = 0;
    //  static uint8_t state = 0;
    // this is the preamble,
    // consisting of 16 ones to trigger the PLL in the TX
    for (int i = 0; i < 15; i++) {
        digitalWrite(LOADMOD_PIN, HIGH);
        delayMicroseconds(250);
        digitalWrite(LOADMOD_PIN, LOW);
        delayMicroseconds(250);
    }

    // this function just calls the byte transmitter
    // and also does the checksum, which is just XOR
    // of all the bits, which is the last item
    bit_state = 0;
    for (int i = 0; i < len; i++) {
        tx_byte(data[i]);
        checksum ^= data[i];
    }
    // transmit the checksum
    tx_byte(checksum);
}

volatile uint8_t  state = 0; // previous code uses this
static uint16_t adcvb[2]; // the 1 length buffer for adc values
uint8_t print_cooldown = 0;

// the loop function runs over and over again forever
void loop() {

    // this is a 10 bit value (i.e. 1-1024)
    // corresponding to 0-3.3V
    // too lazy to check if its safe above that, just have a voltage divider of like 10k/20k
//  Serial.print("ADC says... ");
//  Serial.print(adcv);
//  Serial.println();
//  delay(500);

    uint16_t adcv = adc_read();
    if (adcv < POW_DETECT_THRESHOLD) {
        state = 0;
    }

    switch (state) {
        case 0:
        {
            // we don't have enough power, wait for a ping
            uint16_t adcv = adc_read();
            if (adcv > POW_DETECT_THRESHOLD) {
                state = 1; // we just detected a ping, reply!
                delay(10);

                // remove me later
                Serial.print("Triggered off power");
                Serial.println();
                print_with_cd(adcv);
            }
            print_with_cd(adcv, false);
            break;


        }
        case 1:
        {
            uint8_t dt[] = {
                    0x1,
                    255
            };
            for (int i = 0; i < 20; i++) {
                tx(dt, 2);  //send ping response so that the transmitter identifies receiver.
                delay(10);
            }
            state = 2;

            // remove me later
            Serial.print("Case 1");
            Serial.println();
            print_with_cd(adcv);

            break;
        }

        case 2:
        {

            // if(adcv > ((423*3/2))) {
            int8_t error = 0;
            uint16_t adcv = adc_read();
            int16_t temp_error = 0;
            adcvb[0] = adcvb[1];
            adcvb[1] = adcv;
            //if(abs(adcvb[0] - adcvb[1]) > 20)
            // temp_error = (int16_t)((423* 3) - adcv);
            //else
            temp_error = (int16_t)((1010) - adcv); //1.1v adc reference. 423 equals to 5V. (4.7/47K voltage divider)

            temp_error /= 5;
            if (temp_error > 127) temp_error = 127;
            if (temp_error < -128) temp_error = -128;
            error = (int8_t) temp_error;
            uint8_t dt[] = {
                    0x3,
                    (int8_t) error
            };
            tx(dt, 2);  //send error correction packet. 0x03 is error correction packet header. 1 BYTE payload, check WPC documents for more details.
            /*} else {
             uint8_t dt[] = {0x3,(int8_t)1};
             tx(dt,2);
            }*/
            // remove me later
            Serial.print("Case 2");
            Serial.println();
            print_with_cd(adcv);
        } {
        uint8_t dt[] = {0x4, 0XFF};
        tx(dt, 2);  //received power indication packet. I am not sure if this is needed or not. Please read the WPC document
        //for more details. I jut implemented and it worked. But I am not sure if this is the proper way to do it.

        // remove me later
        Serial.print("mysterious");
        Serial.println();
        print_with_cd(adcv);
    }
            //    _delay_ms(10);
            break;

    }


    // anything below this is just testing/example code
    /*
    digitalWrite(LED_BUILTIN , HIGH);   // turn the LED on (HIGH is the voltage level)
    Serial.print("HIGH");    // prints a tab
    Serial.println();        // prints a carriage return
    delay(1000);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    Serial.print("LOW");    // prints a tab
    delay(1000);                       // wait for a second
    Serial.println();        // prints another carriage return
    */
}